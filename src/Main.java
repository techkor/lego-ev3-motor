import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.robotics.RegulatedMotor;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        RegulatedMotor motorA = new EV3LargeRegulatedMotor(MotorPort.A);
        RegulatedMotor motorB = new EV3LargeRegulatedMotor(MotorPort.B);

        motorA.forward();
        motorB.forward();

        Thread.sleep(1000);

        motorA.stop();
        motorB.stop();
    }

}
